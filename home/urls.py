from django.urls import path
from home import views



app_name = "home"


urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('logout/', views.LogOut.as_view(), name="logout"),
    path('user_profile/', views.CreateUserProfile.as_view(), name="user_profile"),
    path('detail/user_profile/<int:pk>/', views.DetailUserProfile.as_view(), name="detail_userprofile"),
    path('update/user_profile/<int:pk>/', views.UpdateUserProfile.as_view(), name="update_userprofile"),
    path('about/', views.About.as_view(), name="about"),    
    path('identity/', views.Identity.as_view(), name="identity"),
    path('contact/', views.Contact.as_view(), name="contact"),
]