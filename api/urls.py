from django.urls import path
from api import views



app_name = "api"


urlpatterns = [
    path('v2/list/grantgoal/', views.GrantGoalAPIView.as_view(), name="api_list_grantgoal"),
    path('v2/detail/grantgoal/<int:pk>/', views.DetailGrantGoalAPIView.as_view(), name="api_detail_grantgoal"),
    path('v2/create/grantgoal/', views.CreateGrantGoalAPIView.as_view(), name="api_create_grantgoal"),
    

    ##### URLS CLIENTS #####
    path('client/list/grantgoal/', views.ListGrantGoalClient.as_view(), name="clist_grantgoal"),
    path('client/create/grantgoal/', views.CreateGrantGoalClient.as_view(), name="ccreate_grantgoal"),
]