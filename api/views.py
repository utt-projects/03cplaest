from django.shortcuts import render, redirect

# Create your views here.
from django.views import generic

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import GrantGoalSerializer
from core.models import GrantGoal
from .forms import CreateGrantGoalForm



class GrantGoalAPIView(generics.ListAPIView):
    queryset = GrantGoal.objects.filter(status=True)
    serializer_class = GrantGoalSerializer


class DetailGrantGoalAPIView(APIView):

    def get(self, request, pk):
        queryset = GrantGoal.objects.get(pk=pk)
        data = GrantGoalSerializer(queryset).data
        return Response(data)



class CreateGrantGoalAPIView(generics.CreateAPIView):
    serializer_class = GrantGoalSerializer





#### CLIENTS ####


import requests

class ListGrantGoalClient(generic.View):
    template_name = "api/clist_grantgoal.html"
    context = {}
    url_api = "http://localhost:8000/api/v2/list/grantgoal/"

    def get(self, request):
        response = requests.get(url=self.url_api)
        self.context = {
            "grantgoals": response.json()
        }
        return render(request, self.template_name, self.context)



class CreateGrantGoalClient(generic.View):
    template_name = "api/ccreate_grantgoal.html"
    context = {}
    url_api = "http://localhost:8000/api/v2/create/grantgoal/"
    response = None
    payload = {}
    form_class = CreateGrantGoalForm

    def get(self, request):
        self.context = {
            "form": self.form_class
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request, *args, **kwargs):
        self.payload = {                     
            "gg_title": request.POST["gg_title"],
            "description": request.POST["description"],
            "days_duration": request.POST["days_duration"],
            "status": request.POST["status"],
            "state": request.POST["state"],
            "sprint": request.POST["sprint"],
            "slug": request.POST["slug"],
            "user": request.POST["user"]
        }
        self.response = requests.post(url=self.url_api, data=self.payload)
        return redirect("api:clist_grantgoal")