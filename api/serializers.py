


from rest_framework import serializers
from rest_framework import generics

from core.models import GrantGoal


class GrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"